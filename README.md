# Sample NodeJS Services

One paragraph of your services description goes here

---

* [Language](#language)
* [List of API services](#list-of-api-services)
  - [Testing](#testing)
  - [Another](#another)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)

---

## Language

This services are written in **PHP Framework wich are Lumens and Laravel Framework**

## List of API Services

### **Testing**

---

This is just an API for testing.

* **URL**

  ```
  <BASE_URL>/informasispasial/bangunan
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/bangunan
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.611209181593,
                                -6.89157525688781
                            ],
                            [
                                107.611202718968,
                                -6.89156849834835
                            ],
                            [
                                107.611205727965,
                                -6.89155125951686
                            ],
                            [
                                107.611209181593,
                                -6.89157525688781
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 1167,
                "objectid": 1824,
                "shape_leng": 5.65381416424,
                "shape_area": 0.806139619617
            }
        }`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />


================================
* **URL**

  ```
  <BASE_URL>/informasispasial/parkiran
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/parkiran
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.608184428623,
                                -6.88535095272971
                            ],
                            [
                                107.608184521926,
                                -6.88516117847021
                            ],
                            [
                                107.60819022265,
                                -6.8851701842033
                            ],
                            [
                                107.608232607744,
                                -6.88516995288091
                            ],
                            [
                                107.60823359535,
                                -6.88535068438897
                            ],
                            [
                                107.608184428623,
                                -6.88535095272971
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 3,
                "parkira_id": 0,
                "area": 0
            }
        }`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/jalan
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/jalan
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.612048263753,
                                -6.89357799583089
                            ],
                            [
                                107.61205787118,
                                -6.89357794325685
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 1,
                "nm_jalan": null,
                "id_jalan": 1,
                "id_zona": "ITB A"
            }
        }`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/pohon
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/pohon
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `"properties": {
                "id": 49,
                "objectid": 181,
                "area": 44.7481,
                "shape_leng": 23.7736557617,
                "radius": 3.77409096577,
                "height": 13.23322,
                "coord_x": 788622.43191,
                "coord_y": 9237143.93141,
                "diameter": 7.57946,
                "canopydiam": 1.894865
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "MultiPoint",
                "coordinates": [
                    [
                        107.611599721035,
                        -6.89430188689018,
                        777.585600000006
                    ]
                ]
            }`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/taman
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/taman
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.608343517017,
                                -6.8883386411975
                            ],
                            [
                                107.608328390789,
                                -6.88878848734242
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 21,
                "tamanit_id": 0,
                "area": 0
            }
        }`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/trotoar
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/trotoar
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.608587324377,
                                -6.89359692231118
                            ],
                            [
                                107.608513191568,
                                -6.89357925375834
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 15,
                "trotoar_id": 0,
                "area": 0
            }
        }
    ]
}`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/lapangan
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/lapangan
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.609232127208,
                                -6.88583979227882
                            ],
                            [
                                107.608886829727,
                                -6.88584167758028
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 9,
                "lapanga_id": 0,
                "area": 0
            }
        }
    ]
}`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/kolam
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/kolam
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.610369798654,
                                -6.89067285116853
                            ],
                            [
                                107.610370508103,
                                -6.8908024715548
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 9,
                "kolamit_id": 0,
                "area": 0
            }
        }
    ]
}`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/gardu
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/gardu
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.609897133402,
                                -6.89241932244845
                            ],
                            [
                                107.609689506413,
                                -6.89242541687336
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 1614,
                "objectid": 0,
                "shape_leng": 0,
                "shape_area": 0
            }
        }
    ]
}`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

* **URL**

  ```
  <BASE_URL>/informasispasial/koridor
  ```

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  curl -i <BASE_URL>/informasispasial/koridor
  ```

* **Notes:**

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here.

### **Another**

---

Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple).

* **URL**

  ```
  The URL Structure (path only, no root url)
  ```

* **Method:**
  
  The request type

  `GET`
  
* **URL Params**

  If URL params exist, specify them in accordance with name mentioned in URL section. Separate into optional and required. Document data constraints.

   **Required:**
 
   `NONE`

* **Data Params**

  If making a post request, what should the body payload look like? URL Params rules apply here too.

* **Success Response:**
  
  What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!

  * **Code:** 200 <br />
    **Content:** `{
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                107.609841874608,
                                -6.88873518973905
                            ],
                            [
                                107.609843995556,
                                -6.88905345539133
                            ]
                        ]
                    ]
                ]
            },
            "properties": {
                "id": 18,
                "koridor_id": 0,
                "area": 0
            }
        }
    ]
}`
 
* **Error Response:**

  Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />

================================

## Built With

* [Express](https://expressjs.com/) - The web framework used

## Authors

* **Dimas Praja Purwa Aji (10209065)** - [prajadimas](http://178.128.104.74:9000/prajadimas)
* **Novianto Budi Kurniawan (33216028)** - [noviantobudik](http://178.128.104.74:9000/noviantobudik)
* **Gery Reynaldi (23217016)** - [geryreynaldi](http://178.128.104.74:9000/geryreynaldi)
* **CampusGIS Team 2019**

## License

This project is licensed under the MIT License

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc


