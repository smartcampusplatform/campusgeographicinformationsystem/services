<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bangunan extends Model 
{
    
    protected $table = 'bangunan';

    public $timestamps = false;

    // protected $visible = ['id', 'object_id','latlng'];
}
