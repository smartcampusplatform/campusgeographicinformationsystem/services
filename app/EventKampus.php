<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventKampus extends Model 
{
    
    protected $table = 'eventkampus';

    public $timestamps = false;

    // protected $fillable = ['fill'];
    public function bangunan()
    {
        return $this->hasOne('App\Bangunan','id','idbangunan')->select(\DB::raw("
                    id,
                    objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('bangunan') as type
                    "));
    }
}
