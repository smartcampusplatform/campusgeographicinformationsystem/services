<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parkiran extends Model 
{
    
    protected $table = 'parkiran';

    public $timestamps = false;

    protected $fillable = ['fill'];
    
}
