<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ShuttleBus;
use App\PoolBus;

class BusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getInfoBus(){
        $shuttlebus = ShuttleBus::orderBy('id')->get();
        return response()->json(['status' => '200', 'message'=>'success','data' => $shuttlebus]);
    }

    public function getPoolBus(){
        $poolbus = PoolBus::orderBy('id')->get();
        return response()->json(['status' => '200', 'message'=>'success','data' => $poolbus]);
    }

}