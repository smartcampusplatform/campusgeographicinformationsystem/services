<?php

namespace App\Http\Controllers;
// use App\Http\Controllers\DB as DB;
use Illuminate\Http\Request;
use App\Asset;

class AssetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function showAsset(){

    	$asset = Asset::all();

    	return response()->json(['status' => '200', 'message'=>'success','data' => $asset]);
    }

    public function laporAsset(Request $request){

        if($request->id && $request->input('Lapor')){

            $asset = Asset::find($request->id);
            
            $asset->Lapor = $request->input('Lapor');

            $asset->save();

            return response()->json(['status' => '200', 'message'=>'success','data' => $asset]);
        }

        return response()->json(['status' => '404', 'message'=>'no record found']);;
    }

}