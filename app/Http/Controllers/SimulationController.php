<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Parkiran;

class SimulationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $spasial;

    public function __construct(SpasialController $spasial)
    {
        $this->spasial = $spasial;
    }

    public function parkiran(){

        $request = new Request;

        for($i = 0 ; $i<rand(1, 10); $i++){

            $parkiran = Parkiran::where('id' ,'>' ,0)->pluck('id')->toArray();
            
            $request['id'] = array_rand($parkiran);
            $request['operation'] =  rand(1,2);      
      
            // return $request->all();
            $this->spasial->updateParkiran($request);

            // var_dump($request->operation);
        }
        
        $request = new Request;
        $request->search = 'all';

        return $this->spasial->listParkiran($request);



    }
}
