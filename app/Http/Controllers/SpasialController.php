<?php

namespace App\Http\Controllers;
// use App\Http\Controllers\DB as DB;
use Illuminate\Http\Request;
use App\Bangunan;
use App\Parkiran;

class SpasialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

    }

    public function showMap(){
        return $this->listBangunan();
    }

    public function getParkingArea(){

        return $this->listParkiran();
    }

    public function getAssemblyPoint(Request $request){
        return $this->listTaman($request);
    }

    public function listBangunan(Request $request){

        if($request->id){
            $bangunan = \DB::table("bangunan")
                ->select(\DB::raw("
                    id,
                    objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('bangunan') as type
                    "))
                ->where('id','=',$request->id)->get();

            return response()->json(['status' => '200', 'message'=>'success','data' => $bangunan]);
                // return json_encode($bangunan, JSON_PRETTY_PRINT);
        }

        if($request->search){
            
            $bangunan = \DB::table("bangunan")
                ->select(\DB::raw("
                    id,
                    objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('bangunan') as type
                    "))
                ->where('id','LIKE',"%{$request->search}%")->get();

            // return $bangunan;
            return response()->json(['status' => '200', 'message'=>'success','data' => $bangunan]);

        }

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, objectid, shape_leng,shape_area,ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng) As l)) As properties
           FROM bangunan As lg) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listGardu(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, nm_gardu, jns_gardu, tgl_pt, tgl_operas, kp_daya, merk_trafo,id_gardu) As l)) As properties
           FROM gardu As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listJalan(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, nm_jalan,id_jalan,id_zona) As l)) As properties
           FROM jalan As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listKolam(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, kolamit_id, area) As l)) As properties
           FROM kolam As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listKolamSaraga(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, objectid, shape_leng,shape_area) As l)) As properties
           FROM kolamsaraga As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listKontur(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, objectid, shape_leng,shape_area) As l)) As properties
           FROM kontur25cm As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listKoridor(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, koridor_id, area) As l)) As properties
           FROM koridor As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listLapangan(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, lapanga_id, area) As l)) As properties
           FROM lapangan As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listParkiran(Request $request){

        if($request->id){
            $bangunan = \DB::table("parkiran")
                ->select(\DB::raw("
                    id,
                    objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('parkiran') as type
                    "))
                ->where('id','=',$request->id)->get();

            return response()->json(['status' => '200', 'message'=>'success','data' => $parkiran]);
                // return json_encode($bangunan, JSON_PRETTY_PRINT);
        }

        if($request->search){
            
            if($request->search == 'all'){
                $parkiran = \DB::table("parkiran")
                ->select(\DB::raw("
                    id,
                    parkira_id as objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    capacity-fill as remain,
                    CONCAT('parkiran') as type
                    "))
                ->get();
            }
            else {
                $parkiran = \DB::table("parkiran")
                ->select(\DB::raw("
                    id,
                    parkira_id as objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('parkiran') as type
                    "))
                ->where('id','LIKE',"%{$request->search}%")->get();
            }
            

            // return $parkiran;
            return response()->json(['status' => '200', 'message'=>'success','data' => $parkiran]);

        }

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, parkira_id, area) As l)) As properties
           FROM parkiran As lg ) As f;
        ");
        
        return $result = $this->geoJson($query,'allfeatures');

    }

    public function updateParkiran(Request $request){

        if($request->input('id') && $request->input('operation')){
            $parkiran = Parkiran::find($request->input('id'));

            if($request->input('operation') == 2 && $parkiran->fill > 0){
                $parkiran->fill = $parkiran->fill - 1;
            }else if ($request->input('operation') == 1 && $parkiran->fill < $parkiran->capacity){
                $parkiran->fill = $parkiran->fill + 1;
            }

            $parkiran->save();

            return response()->json(['status' => '200', 'message'=>'success','data' => $parkiran]);
        }

        return 'zonk';
        // return $request->input('operation');
    }

    public function listPohon(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, objectid, area, shape_leng, radius, height, coord_x, coord_y, diameter, canopydiam) As l)) As properties
           FROM pohon As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listTaman(Request $request){


        if($request->id){
            $bangunan = \DB::table("taman")
                ->select(\DB::raw("
                    id,
                    tamanit_id as objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('evacuation area') as type
                    "))
                ->where('id','=',$request->id)->get();

            return response()->json(['status' => '200', 'message'=>'success','data' => $taman]);
                // return json_encode($bangunan, JSON_PRETTY_PRINT);
        }

        if($request->search){
            
            if($request->search == 'all'){
                $taman = \DB::table("taman")
                ->select(\DB::raw("
                    id,
                    tamanit_id as objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('evacuation area') as type
                    "))
                ->get();
            }
            else {
                $taman = \DB::table("taman")
                ->select(\DB::raw("
                    id,
                    tamanit_id as objectid,
                    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) as lat,
                    ST_X(ST_Centroid(ST_Transform(geom, 4326))) as lng,
                    CONCAT('evacuation area') as type
                    "))
                ->where('id','LIKE',"%{$request->search}%")->get();
            }
            

            // return $parkiran;
            return response()->json(['status' => '200', 'message'=>'success','data' => $taman]);

        }

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, tamanit_id, area) As l)) As properties
           FROM taman As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function listTrotoar(){

        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geom, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, trotoar_id, area) As l)) As properties
           FROM trotoar As lg ) As f;
        ");
        
           return $result = $this->geoJson($query,'allfeatures');

    }

    public function geoJson ($locales, $opsi){

        $original_data = json_decode(json_encode($locales), true);
        $features = array();

        $features = json_decode($original_data[0]['features'],true);

        if($opsi == 'allfeatures'){
            $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);
            return json_encode($allfeatures, JSON_PRETTY_PRINT);
        }

        return json_encode($features, JSON_PRETTY_PRINT);
    }
}
