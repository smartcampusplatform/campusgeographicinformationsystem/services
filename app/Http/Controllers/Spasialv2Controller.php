<?php

namespace App\Http\Controllers;
// use App\Http\Controllers\DB as DB;
use Illuminate\Http\Request;
use App\BUilding;
use App\BuildingMap;

class Spasialv2Controller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

    }

    public function showMap(){

        // $map = BuildingMap::with('building')->get();

        // return $map;
        $query = app('db')->select("
            SELECT 'FeatureCollection' As type,  array_to_json(array_agg(f)) As features
            FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(ST_Transform(lg.geometry, 4326))::jsonb As geometry
                , row_to_json((SELECT l FROM (SELECT id, master) As l)) As properties
           FROM detail_building_geom As lg) As f;
        ");
        
        return $result = $this->geoJson($query,'allfeatures');
    }

    

    public function geoJson ($locales, $opsi){

        $original_data = json_decode(json_encode($locales), true);
        $features = array();

        $features = json_decode($original_data[0]['features'],true);

        if($opsi == 'allfeatures'){
            $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);
            return json_encode($allfeatures, JSON_PRETTY_PRINT);
        }

        return json_encode($features, JSON_PRETTY_PRINT);
    }
}
