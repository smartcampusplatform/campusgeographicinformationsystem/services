<?php

namespace App\Http\Controllers;
// use App\Http\Controllers\DB as DB;
use Illuminate\Http\Request;
use App\Bangunan;
use App\EventKampus;

class EventKampusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function showEvent(){

    	$event = EventKampus::orderBy('id')->with('bangunan')->get();

        // $collection = collect($event);

        // $collection = $collection->pluck('bangunan');
        // $collection = $collection->pull('lat');

        // return $collection;

        $array =  array();
        $i = 0;
        foreach($event as $key=>$value){

            $collection = collect($value);
            $collection = $collection->merge($value->bangunan);
            $array[$i] = $collection;
            // echo $collection;
            $i++;
        }

        // return $array;
    	return response()->json(['status' => '200', 'message'=>'success','data' => $array]);
    }


}