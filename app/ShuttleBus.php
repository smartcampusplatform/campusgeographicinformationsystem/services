<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShuttleBus extends Model 
{
    
    protected $table = 'shuttlebus';

    public $timestamps = false;
   
}
