<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model 
{
    
    protected $table = 'building';

    public $timestamps = false;

    // protected $visible = ['id', 'object_id','latlng'];
}
