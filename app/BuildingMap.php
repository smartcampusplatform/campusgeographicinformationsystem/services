<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildingMap extends Model 
{
    
    protected $table = 'detail_building_geom';

    public $timestamps = false;

    // protected $visible = ['id', 'object_id','latlng'];

    public function building()
    {
        return $this->hasOne('App\Building','id','master');
    }
}
