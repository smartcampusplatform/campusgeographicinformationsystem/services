<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model 
{
    
    protected $table = 'asset';

    protected $primaryKey = "Id";

    public $timestamps = false;

    protected $fillable = ['Lapor'];
    
}
