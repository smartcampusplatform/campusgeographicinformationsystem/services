<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoolBus extends Model 
{
    
    protected $table = 'poolbus';

    public $timestamps = false;
   
}
