<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'informasispasial'], function () use ($router) {

    $router->get('/', ['as' => 'showMap', 'uses' => 'SpasialController@showMap']);
    $router->get('/bangunan', ['as' => 'listBangunan', 'uses' => 'SpasialController@listBangunan']);
    $router->get('/gardu', ['as' => 'listBangunan', 'uses' => 'SpasialController@listBangunan']);
    $router->get('/jalan', ['as' => 'listJalan', 'uses' => 'SpasialController@listJalan']);
    $router->get('/kolam', ['as' => 'listKolam', 'uses' => 'SpasialController@listKolam']);
    $router->get('/kolamsaraga', ['as' => 'listKolamsaraga', 'uses' => 'SpasialController@listKolamsaraga']);
    $router->get('/kontur', ['as' => 'listKontur', 'uses' => 'SpasialController@listKontur']);
    $router->get('/koridor', ['as' => 'listKoridor', 'uses' => 'SpasialController@listKoridor']);
    $router->get('/lapangan', ['as' => 'listLapangan', 'uses' => 'SpasialController@listLapangan']);

    $router->get('/parkiran', ['as' => 'listParkiran', 'uses' => 'SpasialController@listParkiran']);
    $router->put('/parkiran', ['as' => 'updateParkiran', 'uses' => 'SpasialController@updateParkiran']);

    $router->get('/pohon', ['as' => 'listPohon', 'uses' => 'SpasialController@listPohon']);
    $router->get('/pointbm', ['as' => 'listPointbm', 'uses' => 'SpasialController@listPointbm']);
    $router->get('/taman', ['as' => 'listTaman', 'uses' => 'SpasialController@listTaman']);
    $router->get('/trotoar', ['as' => 'listTrotoar', 'uses' => 'SpasialController@listTrotoar']);
    $router->get('/evacuationarea', ['as' => 'getAssemblyPoint', 'uses' => 'SpasialController@getAssemblyPoint']);
});

$router->group(['prefix' => 'api/v2/informasispasial'], function () use ($router) {

    $router->get('/', ['as' => 'showMap', 'uses' => 'Spasialv2Controller@showMap']);
});

$router->group(['prefix' => 'personalisasi'], function () use ($router) {

    $router->get('/eventkampus', ['as' => 'showevent', 'uses' => 'EventKampusController@showevent']);
    $router->get('/shuttlebus', ['as' => 'getinfobus', 'uses' => 'BusController@getInfoBus']);
    $router->get('/poolbus', ['as' => 'getpoolbus', 'uses' => 'BusController@getPoolBus']);
});

$router->group(['prefix' => 'asset'], function () use ($router) {
    $router->get('/', ['as' => 'showasset', 'uses' => 'AssetController@showAsset']);
    $router->put('/{id}', ['as' => 'lapor', 'uses' => 'AssetController@laporAsset']);
});

$router->group(['prefix' => 'simulasi'], function () use ($router) {

    $router->get('/parkiran', ['as' => 'lists', 'uses' => 'SimulationController@parkiran']);
});


